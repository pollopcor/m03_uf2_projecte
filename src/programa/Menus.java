package programa;

public class Menus {

	// MENU PRINCIPAL
	static int MenuPrincipal() {

		// System.out.println("<Poner aqui un ascii art>");

		System.out.println();

		System.out.println(" .====== MENU ======.");
		System.out.println(" |------------------|");
		System.out.println(" | 1. Jugar         |");
		System.out.println(" | 2. Info          |");
		System.out.println(" | 0. Sortir        |");
		System.out.println(" *------------------*");
		System.out.print("   Opcion: ");

		return Joc.scn.nextInt();
	}

	// MENU CON OPCIONES PARA CREAR PERSONAJE
	public static int MenuModificarPersonaje(Personajes.Persona p) {

		Funciones.Consola.clear();

		System.out.println("\n.===== Crear personaje ====.");
		System.out.print("| Nombre: ");
		System.out.print(Funciones.Consola.espaciadoDerecha(Funciones.PersonajesF.verNombre(p), 17) + "|\n");
		System.out.println("|==========================|");
		System.out.println("|  1. Cambiar nombre       |");
		System.out.println("|  2. Proximamente         |");
		System.out.println("|  0. Guardar              |");
		System.out.println("| -1. Salir sin guardar    |");
		System.out.println("|__________________________|");
		return Joc.scn.nextInt();

	} // Fi menu - crear personaje

	// Classe Casa
	// - Contiene los submenus con las opciones de nuestra casa/base
	public static class Casa {

		// MENU PRINCIPAL
		public static int menuPrincipal(Personajes.Persona p, int dia, int minutos) {

			int min_usados = -1;

			// Cuando tiempo_usado toma un valor:
			// Si es 0, se ha echo una accion que no consume tiempo, retornar
			// Si es >0, se ha echo una accion que consume tiempo, retornar
			// Si es -1, se ha vuelto atras desde un submenu, ense�a el menu de nuevo

			do {

				Funciones.Consola.clear();

				System.out.println("####### Dia " + dia + " Hora " + Funciones.Conversiones.minutosATiempo(minutos)
						+ " #######\n");

				System.out.println(Funciones.PersonajesF.informacion(p));

				System.out.println(" ____________________________");
				System.out.println("|            MENU            |");
				System.out.println("|============================|");
				System.out.println("| 1. Casa >                  |");
				System.out.println("| 2. Explorar >              |");
				System.out.println("| 3. Grupo >                 |");
				System.out.println("| 4. Inventario >            |");
				System.out.println("| 5. Esperar >               |");
				System.out.println("| 0. Salir al menu principal |");
				System.out.println(" ----------------------------");

				System.out.println("\nElije una opcion:");

				switch (Joc.scn.nextInt()) {

				case 1:
					min_usados = Menus.Casa.Interior();
					break;

				case 2:
					min_usados = Menus.Casa.Exploraciones();
					break;

				case 3:
					// return Menus.MenuGrupo();
					break;

				case 4:
					Funciones.Inventario.administrarInventario(p.Inventario, null, true);
					break;

				case 5:
					min_usados = Menus.General.menuEsperar();
					break;

				case 0:
					// Si el usuario quiere salir al menu principal
					//// Joc.scn.nextLine(); //Fix
					if (Funciones.Datos.preguntar("Seguro que quieres salir?", true)) {
						Funciones.Consola.clear();
						return -1; // Saldra de la partida
					} else
						break;

				default:
					System.out.println("Elije una opcion valida!");

				}

				// Si es >= 0, se ha echo una accion que consume o no minutos
				// Si es -1, se ha vuelto atras desde un submenu (ense�ara el menu de nuevo)
			} while (min_usados == -1);

//			//Si se han gastado, haz una pausa
//			if(min_usados > 0)
//				Funciones.Consola.pausa(2);

			// Devuelve tiempo usado
			return min_usados;

		} // Fin menu - menu principal

		// SUM-MENU -- CASA >>> Interior
		// - Si devuelve 0 ha echo una accion que no consume tiempo
		// - Si devuelve -1, se quiere volver al menu anterior
		public static int Interior() {

			Funciones.Consola.clear();

			System.out.println(" _________________");
			System.out.println("| Casa            |");
			System.out.println("|=================|");
			System.out.println("| 1. Habitacion > |");
			System.out.println("| 2. Inventario > |");
			System.out.println("| 3. Cocina >     |");
			System.out.println("| 4. ----         |");
			System.out.println("| 0. < Atras      |");
			System.out.println(" -----------------");

			System.out.println("\nElije una opcion:");

			while (true) {

				switch (Joc.scn.nextInt()) {

				case 1:
					// SUB-MENU - Ir a la habitacion
					return Menus.Casa.InteriorHabitacion();

				case 2:
					// SUB-MENU - Ver inventario
					return Menus.Casa.InteriorInventario();
				// return Menus.MenuCasaInventario();

				case 3:
					// SUB-MENU - Ir a la cocina
					return Menus.Casa.InteriorCocina();

				case 0:
					return -1;

				default:
					System.out.println("- Elije una opcion valida.");

				}
			}

		}

		// SUB-MENU -- CASA >>> Habitacion
		public static int InteriorHabitacion() {

			Funciones.Consola.clear();

			System.out.println(" ____________________");
			System.out.println("| Casa > Habitacion: |");
			System.out.println("|====================|");
			System.out.println("| 1. Dormir [2h]     |");
			System.out.println("| 2. Proximamente    |");
			System.out.println("| 3. Proximamente    |");
			System.out.println("| 4. Proximamente    |");
			System.out.println("| 0. < Atras         |");
			System.out.println("---------------------");

			System.out.println("\nElije una opcion:");

			while (true) {

				switch (Joc.scn.nextInt()) {

				case 1:
					// ACCION - Dormir (2h)
					return Funciones.Conversiones.horasAMinutos(2);

				case 2:
					// ACCION - Proximamente
					return 0;

				case 0:
					return -1;

				default:
					System.out.println("- Elije una opcion valida.");
				}

			}

		}

		// SUB-MENU -- CASA >>> Inventario
		public static int InteriorInventario() {

			Funciones.Consola.clear();

			System.out.println(" _____________________________");
			System.out.println("| Casa > Inventario:          |");
			System.out.println("|=============================|");
			System.out.println("| 1. Administrar inventario > |");
			System.out.println("| 0. < Atras                  |");
			System.out.println(" -----------------------------");

			System.out.println("\nElije una opcion:");

			while (true) {

				switch (Joc.scn.nextInt()) {

				case 1:
					// ACCION - Administrar inventario
					Funciones.Inventario.administrarInventario(Datos.PERSONAJE.Inventario, Datos.CASA.Inventario, true);
					return 15; // 15 minutos usados

				case 0:
					return -1;

				default:
					System.out.println("- Elije una opcion valida.");
				}

			}

		}

		// SUB-MENU -- CASA >>> Cocina
		public static int InteriorCocina() {

			Funciones.Consola.clear();

			System.out.println(" _____________________");
			System.out.println("| Casa > Cocina:      |");
			System.out.println("|=====================|");
			System.out.println("| 1. Cocinar [15 min] |");
			System.out.println("| 2. Mejorar          |");
			System.out.println("| 3. Proximamente     |");
			System.out.println("| 4. Proximamente     |");
			System.out.println("| 0. < Atras          |");
			System.out.println("----------------------");

			System.out.println("\nElije una opcion:");

			while (true) {

				switch (Joc.scn.nextInt()) {

				case 1:
					Joc.scn.nextLine();
					// ACCION - Cocinar
					// Devuelve =>Numero de items cocinados * 15min (15min en cocinar cada item)
					return programa.Funciones.CasaF.Cocina.CocinarItems(Datos.PERSONAJE.Inventario) * 15;

				case 2:
					// ACCION - Mejorar cocina
					System.out.println("<Proximamente>");
					return 0;

				case 0:
					return -1;

				default:
					System.out.println("- Elije una opcion valida.");
				}

			}

		}

		// SUB-MENU -- CASA >>> Explorar
		public static int Exploraciones() {

			Funciones.Consola.clear();

			System.out.println(" ______________________");
			System.out.println("| Casa > Explorar      |");
			System.out.println("|======================|");
			System.out.println("| 1. Sitios >          |");
			System.out.println("| 2. Ver inventario >  |");
			System.out.println("| 3. Acompanyante >    |");
			System.out.println("| 4. Ir a explorar     |");
			System.out.println("| 0. < Atras           |");
			System.out.println(" ---------------------- ");

			System.out.println("\nElije una opcion:");

			while (true) {

				switch (Joc.scn.nextInt()) {

				case 1:
					// return Menus.MenuExplorarSitios();
					System.out.println("<Proximamente>");
					break;

				case 2:
					Funciones.Inventario.mostrarInventarioDetallado(Datos.PERSONAJE.Inventario);
					break;

				case 3:
					Menus.General.menuAcompanyante(); // Llevar mascota
					break;

				case 4: // Saldra a comenzarDia (Personaje.explorando estara en True y entrara al menu
						// de exploracion)
					if (Funciones.Datos.preguntar("> Ir a explorar?", true)) {
						programa.Datos.PERSONAJE.explorando = true;
						return 0;
					}
					break;

				case 0:
					return -1;

				default:
					System.out.println("- Elije una opcion valida.");
				}
			}
		}

	} // Fin classe CASA

	// Clase Explorar
	public static class Explorar {

		public static int menuPrincipal(Personajes.Persona p, int dia, int minutos) {

			int min_usados = -1;

			// Cuando tiempo_usado toma un valor:
			// Si es 0, se ha echo una accion que no consume tiempo, retornar
			// Si es >0, se ha echo una accion que consume tiempo, retornar
			// Si es -1, se ha vuelto atras desde un submenu, ense�a el menu de nuevo

			do {

				Funciones.Consola.clear();

				System.out.println("####### Dia " + dia + " Hora " + Funciones.Conversiones.minutosATiempo(minutos)
						+ " #######\n");

				System.out.println(Funciones.PersonajesF.informacion(p));

				System.out.println(" ____________________________");
				System.out.println("|            MENU            |");
				System.out.println("|============================|");
				System.out.println("| 1. Seguir explorando >     |");
				System.out.println("| 2. Inventario >            |");
				System.out.println("| 3. Volver a casa >         |");
				System.out.println("| 4. ----- >                 |");
				System.out.println("| 0. Salir al menu principal |");
				System.out.println(" ----------------------------");

				System.out.println("\nElije una opcion:");

				switch (Joc.scn.nextInt()) {

				case 1:
					// min_usados = Menus.Explorar.Explorar();
					Joc.scn.nextLine();
					Eventos.eventoAleatorio(p, dia);
					break;

				case 2:
					// min_usados = Menus.Explorar.Accion();
					Funciones.Inventario.administrarInventario(p.Inventario, null, true);
					break;

				case 3:
					if (Funciones.Datos.preguntar("> Volver a casa?", true)) {
						programa.Datos.PERSONAJE.explorando = false;
						return 0;
					}

					break;

				case 4:
					// min_usados = Menus.Explorar.Accion();
					break;

				case 0:
					// Si el usuario quiere salir al menu principal
					if (Funciones.Datos.preguntar("Seguro que quieres salir?", true))
						return -1; // Saldra de la partida
					else
						break;

				default:
					System.err.println("# Elije una opcion valida!");

				}

				// Si es >= 0, se ha echo una accion que consume o no minutos
				// Si es -1, se ha vuelto atras desde un submenu (ense�ara el menu de nuevo)
			} while (min_usados == -1);

			// Devuelve tiempo usado
			return min_usados;

		} // Fin menu - menu principal

	} // Fin classe EXPLORAR

	public static class General {

		// MENU ESPERAR
		public static int menuEsperar() {

			Joc.scn.nextLine();

			Funciones.Consola.clear();

			System.out.println("  _____________");
			System.out.println(" |   Esperar   |");
			System.out.println(" |-------------|");
			System.out.println(" | 0. Atras    |");
			System.out.println("  -------------");
			System.out.println(" Puedes esperar algunos minutos o horas [Maximo: 2h]");
			System.out.println(" Por defecto seran horas, puedes especificar minutos poniendo \"min\" al final");
			System.out.println("\nCuanto tiempo quieres esperar? (Horas)");

			int minutos = -1;
			boolean en_horas = true;
			String tiempo_str;

			do {

				// Lee el tiempo como string i quita los espacios en blanco de los extremos
				tiempo_str = Joc.scn.nextLine().trim().toLowerCase();

				// Si quiere salir
				if (tiempo_str.equals("0"))
					return -1;

				// Pasar tiempo especificado a minutos
				try {

					// Por defecto el numero especificado seran horas
					en_horas = true;
					minutos = 0;

					// Mirar si se especifica en minutos
					if (tiempo_str.endsWith("min")) {

						// Quitar el "min" del final
						tiempo_str = tiempo_str.substring(0, tiempo_str.length() - 3).trim();
						en_horas = false; // Ya esta en minutos

					} else if (tiempo_str.endsWith("h")) {
						// Quitar el "h" del final
						tiempo_str = tiempo_str.substring(0, tiempo_str.length() - 1).trim();
					}

					// Pasar a int
					minutos = Integer.parseInt(tiempo_str);

					// Si son horas, pasar a minutos
					if (en_horas) {
						minutos = Funciones.Conversiones.horasAMinutos(minutos);
					}

					// Evitar que pase de las 2h y sea menos de 1 minuto
					if (minutos <= 0) {
						System.out.println("- No puedes esperar menos de 1 minuto");
						minutos = 0;

					} else if (minutos > 120) {
						System.out.println("- No puedes esperar m�s de 2h");
						minutos = 0;
					}

				} catch (Exception e) {
					// Da error, que continue la vuelta
					System.out.println("- El formato del tiempo no es correcto, introduze un numero positivo");
					System.out.println("  Info: Puedes poner \"min\" al final para indicarlo en minutos");
				}

			} while (minutos == 0);

			// Mostrar lo que ha esperado el usuario
			System.out.print("@ Has esperado ");
			if (en_horas)
				System.out.println(tiempo_str + " horas");
			else
				System.out.println(minutos + " minutos");

			return minutos;
		}

		// MENU ACOMPA�ANTE
		public static void menuAcompanyante() {

			int opcion = 0;
			Personajes.Persona p = Datos.PERSONAJE;

			do {

				Funciones.Consola.clear();

				System.out.println("  ________________");
				System.out.println(" |  Acompa�antes  |");
				System.out.println(" |----------------|");
//				System.out.print(" | Actual: ");
//				if (p.verMascota() == null)
//					System.out.println("Ninguno");
//				else
//					System.out.println(p.verMascota().nombre + " (" + p.verMascota().tipo + ")");
//				System.out.println(" | -------------- |");

				// Mostrar mascotas disponibles y equipada
				int i = 1;
				for (Personajes.Animal mascota : Datos.MASCOTAS.Miembros) {

					System.out.print(" | " + i + ". " + mascota.nombre + " (" + mascota.tipo + ") ");

					// Si es la mascota equipada, mostrala com equipada
					if (mascota == p.verMascota())
						System.out.print("[Equipada]");

					System.out.println();
					i++;
				}

				System.out.println(" | 0. Atras       |");
				System.out.println("  ----------------");

				System.out.print("\nEscojer acompa�ante: ");
				opcion = Joc.scn.nextInt();
				System.out.println();

				switch (opcion) {

				case 0:
					break;

				case -1:
					p.dejarMascota();
					break;

				default:
					// Que sea un item valido
					if (opcion > Datos.MASCOTAS.Miembros.size() || opcion < -1) {
						System.err.println("# Escoje uno de la lista!");
						break;
					}

					// Si seleccionamos la equipada, la desequipa
					if (p.verMascota() == Datos.MASCOTAS.Miembros.get(opcion - 1)) {
						p.dejarMascota();
					} else {
						p.assignarMascota(Datos.MASCOTAS.Miembros.get(opcion - 1));
					}
				}

			} while (opcion == 0);

			Datos.PERSONAJE = p;

		}

	}

}
