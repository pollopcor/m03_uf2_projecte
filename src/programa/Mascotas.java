package programa;

import java.util.ArrayList;

public class Mascotas {

	ArrayList<Personajes.Animal> Miembros = new ArrayList<Personajes.Animal>();

	public Mascotas() {

	}

	// AŅADIR MIEMBRO
	public void aņadirMascota(Personajes.Animal animal) {
		Miembros.add(animal);
	}

	// QUITAR MIEMBRO
	public void quitarMascota(Personajes.Animal animal) {
		Miembros.remove(animal);
	}

	// QUITAR MIEMBRO POR INDICE
	public int quitarMascotaIndice(int posicion) {

		try {
			Miembros.remove(posicion);
			return 0;

		} catch (IndexOutOfBoundsException error) { // Posicion sale de la lista, no existe
			return 404;

		} catch (Exception e) {
			return 400;
		} // Error general
	}

}
