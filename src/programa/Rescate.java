package programa;

public class Rescate {

	static int Dia = 30 + (int) (Math.random() * 40 + 1);

	// PROLONGAR EL DIA DEL RESCATE
	public static void prolongarRescate() {

		//Sumar dias al dia de rescate
		Dia += 20 + (int) (Math.random() * 10 + 1);
	}
	
	// PREGUNTAR PARA SER RESCATADO
	public static boolean preguntarRescate() {
		
		if(Funciones.Datos.preguntar("Quieres ser rescatado?", true))
			return true;
		else
			return false;
	}

}
