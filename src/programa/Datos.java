package programa;

import java.util.ArrayList;

import programa.Items.Comida;
import programa.Items.Arma;
import programa.Personajes;

public class Datos {

	public static Casa CASA = new Casa();
	public static Personajes.Persona PERSONAJE = null;
	public static Grupo GRUPO = new Grupo();
	public static Mascotas MASCOTAS = new Mascotas();

	public static Personajes.Animal PERRO = new Personajes.Animal("Perro", "Perro");

	// Lista con todos los items del juego
	public static class Lista {

		// Lista de comida
		public static ArrayList<Comida> Comida = new ArrayList<Comida>();

		// Lista de armas
		public static ArrayList<Arma> Armas = new ArrayList<Arma>();

		// Lista de personajes
		public static ArrayList<Personajes.Persona> Personas = new ArrayList<Personajes.Persona>();

		// Lista de acompa�antes
		public static ArrayList<Personajes.Animal> Animales = new ArrayList<Personajes.Animal>();

	}

	// CARGAR OBJETOS
	public static void Cargar() {

		// NOTA: Cargar desde un archivo, es mas facil y ocupa menos codigo

		// Posible mejora: Random, leer directamente desde un fichero y crear el objeto
		// al a�adirlo a un inventario

		CargarComida();
		CargarArmas();
		CargarPersonajes();

		// TEST ===========================================================
		CASA.Inventario.A�adir(new Items.Comida("Alubias", 45));
		CASA.Inventario.A�adir(new Items.Comida("Arroz", 40));
		CASA.Inventario.A�adir(new Items.Comida("Carne", 35));
		CASA.Inventario.A�adir(new Items.Comida("Conejo", 25));

		PERSONAJE.Inventario.A�adir(new Items.Comida("Sopa", 35));
		PERSONAJE.Inventario.A�adir(new Items.Comida("Leche", 20));
		PERSONAJE.Inventario.A�adir(new Items.Comida("Filete", 25));
		PERSONAJE.Inventario.A�adir(new Items.Comida("Zahanoria", 10));
		// ===========================================================
	}

	// CARGAR COMIDA
	public static void CargarComida() {

		// Cargar comida
		Lista.Comida.add(new Comida("Lata de espagetis", 25));
		Lista.Comida.add(new Comida("Manzana", 10));

	}

	// CARGAR ARMAS
	public static void CargarArmas() {

		// Cargar armas
		Lista.Armas.add(new Arma("Glock 18", 8));
		Lista.Armas.add(new Arma("AR-15", 15));

	}

	// CARGAR PERSONAJES
	public static void CargarPersonajes() {

		// Cargar personajes
		Lista.Personas.add(new Personajes.Persona("Jhon"));
		Lista.Personas.add(new Personajes.Persona("Sara"));

	}

	// Cargar acompa�antes
	public static void CargarAnimales() {

		Lista.Animales.add(new Personajes.Animal("Perro", "perro"));

	}

	// RANDOM ITEM DE LA LISTA
	public static class Aleatorio {

		// Item aleatorio
		public static Items.Item Item() {

			int r = (int) (Math.random() * 10);

			// Dar mas probabilidad a obtener comida
			if (r > 3)
				return Datos.Aleatorio.Comida();
			else // if (r > 0)
				return Datos.Aleatorio.Arma();
		}

		// Comida aleatoria
		public static Items.Comida Comida() {
			return Lista.Comida.get((int) (Math.random() * Lista.Comida.size()));
		}

		// Arma aleatoria
		public static Items.Arma Arma() {
			return Lista.Armas.get((int) (Math.random() * Lista.Armas.size()));
		}

		// Personaje aleatorio
		public static Personajes.PersonajeBase Personaje() {
			return Lista.Personas.get((int) (Math.random() * Lista.Personas.size()));
		}

		// Animal aleatorio
		public static Personajes.Animal Animal(String _tipo) {

			_tipo = _tipo.toLowerCase().trim();

			if (_tipo.equals("*") || _tipo.equals(""))
				return Lista.Animales.get((int) (Math.random() * Lista.Animales.size()));
			else {

				ArrayList<Integer> indexTipos = new ArrayList<Integer>();

				// Coje todos los index con objetos de ese tipo
				for (int i = 0; i < Lista.Animales.size(); i++) {
					if (Lista.Animales.get(i).tipo.equals(_tipo))
						indexTipos.add(i);
				}

				if (indexTipos.size() > 0) {

					// De los indizes validos, calcula uno aleatorio
					Integer index = indexTipos.get((int) (Math.random() * indexTipos.size()));

					return Lista.Animales.get(index);
				} else
					return null;
			}
		}

	} // FIN CLASE - Random

}
