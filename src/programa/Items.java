package programa;

public class Items {

	//Info:
	//- Create base class and extend it to subclasses
	//  https://stackoverflow.com/questions/26258888/add-subclass-objects-to-an-arraylist
	//- Change superclass value from subclass
	//  https://stackoverflow.com/questions/27532532/changing-variable-values-in-subclasses
	
	//Clase base para los items
	public static class Item {
		public String nombre;
		public String tipo;
		
		public Item(String nombre, String tipo) {
			
			if(nombre.length() > 20) {
				nombre = nombre.substring(0, 19);
			}
			
			this.nombre = nombre;
			this.tipo = tipo.toLowerCase().trim();
		}
		
		
		
	}
	
	//Clase que adquiere las propiedades de la clase base
	public static class Comida extends Items.Item {

		int Recupera = 10; 						//Cantidad que recupera
		boolean Cocinado = false; 				//Esta cocinado (Si/no)

		// Constructor del item Comida
		public Comida(String nombre, int recupera) {
			super(nombre, "comida"); //Llama al constructor de la clase base
			this.Recupera = recupera;
		}
		
		//Constructor con cocinado
		public Comida(String nombre, int recupera, boolean cocinado) {
			
			this(nombre, recupera);
			this.Cocinado = cocinado;
		}

	}

	public static class Arma extends Items.Item {

		// Valores por defecto
		int Ataque = 5;

		// Constructor del item Arma
		public Arma(String nombre, int ataque) {
			super(nombre, "arma");
			this.Ataque = ataque;
		}
	}

	
	
	
}
