package programa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Funciones {

	////////////////////////////
	// CLASE - HISTORIA
	////////////////////////////
	// - Contiene funciones para obtener/modificar datos
	public static class Historia {

		// LEER INTRODUCCION ALEATORIA
		public static void IntroduccionAleatoria() {
			Funciones.Consola.clear();
			// Elije una introduccion aleatoria y muestrala con formato
			Datos.leerFichero(Datos.ficheroAleatorio(Config.Directorios.introducciones));
		}

	}

	////////////////////////////
	// CLASE - DATOS
	////////////////////////////
	// - Contiene funciones para obtener/modificar datos
	public static class Datos {

		// PREGUNTAR (SI/NO)
		public static boolean preguntar(String pregunta, boolean fix) {

			String res;

			// Si antes se ha pedido un Int, usar esto para que no lo salte
			if (fix)
				Joc.scn.nextLine();

			System.out.println(pregunta + " [S/N]:");
			while (true) {
				res = Joc.scn.nextLine().toLowerCase();

				if (res.length() > 0) {
					switch (res.charAt(0)) {
					case 'y':
					case 's':
						return true;

					case 'n':
						return false;

					default:
						System.err.println("- Escoje una opcion correcta");
					}

				} else {
					System.err.println("# Introduce una respuesta.");
				}
			}
		}

		// PREGUNTAR NUEVO VALOR DE VARIABLES STRING
		public static String cambiarValor(String actual, boolean fix) {

			System.out.println();

			// Si antes se ha pedido un Int, usar esto para que no lo salte
			if (fix)
				Joc.scn.nextLine();

			// Si ya tenia un valor, muestralo
			if (actual != null)
				System.out.println("\n Actual: " + actual);

			System.out.println(" Nuevo: ");
			String nuevo = Joc.scn.nextLine();

			// Si esta vacio, retorna el valor actual
			if (nuevo.equals(""))
				return actual;
			else
				return nuevo;

		}

		// QUALIDAD SEGUN EL VALOR
		// - Decir la cualidad de la caracteristca segun el valor
		public static String estadoSegunValor(int valor, boolean masculino) {

			String estado;

			if (valor == 100)
				estado = "complet";
			else if (valor >= 75)
				estado = "alt";
			else if (valor >= 50)
				estado = "medi";
			else
				estado = "baj";

			if (masculino)
				return estado + "o";
			else
				return estado + "a";

		}

		// GRAFICO SEGUN VALOR
		public static String graficoSegunValor(double actual, int total) {

			String g;
			int porcentaje = (int) (actual * 100 / total); // Porcentaje de la linea
			int porcentaje2 = (int) (porcentaje * 20 / 100); // Numero de caracteres a pintar (de 20 en total)

			g = "[";
			for (int i = 0; i < 20; i++) {

				if (i < porcentaje2)
					g += "=";
				else if (i == porcentaje2)
					g += ">";
				else
					g += ":";

			}

			g += "] " + porcentaje + "%\n";

			return g;
		}

		// COJER IDS DEL COMANDO
		private static ArrayList<Integer> cojerIDs(String[] cmd, boolean cojer_primero) {

			ArrayList<Integer> IDs = new ArrayList<Integer>();

			// La primera posicion podria ser un comando o una ID
			// Si va a ser una ID, podemos indicarle que tambien lo coja
			int j = 1;
			if (cojer_primero)
				j = 0;

			// Por cada argumento
//			int itemID;
			for (int i = j; i < cmd.length; i++) {

				IDs.add(StringANumero(cmd[i]));

//				try {
//
//					// Intenta pasarlo a Integer
//					itemID = Integer.parseInt(cmd[i]);
//
//					IDs.add(itemID);
//
//				} catch (NumberFormatException error) {
//					// Si falla con la excepcion NumberFormat, se pondra a negativo/null
//					IDs.add(-1);
//				}
			}

			return IDs;
		}

		public static int StringANumero(String linea) {
			int num;
			try {
				// Intenta pasar String a Int
				num = Integer.parseInt(linea);
				return num;

			} catch (NumberFormatException error) {
				// Si falla con la excepcion NumberFormat, se pondra a 0
				return 0;
			}
		}

		// LEER FICHERO
		public static void leerFichero(String ruta) {

			// Legenda:
			// # => Comentario
			// ;X => Pausa de X segundos
			// : => No hacer pausa
			// %nombre% => Nombre del personaje
			// ~ => Enter para seguir
			// , => Tabulado y sin pausa (para dibujos)
			// - => Linea vacia y sin pausa

			// Para a�adir:
			// >Texto => Preguntar

			// Si no se ha podido escojer un archivo valido
			if (ruta == null) {
				System.err.println("# Ningun archivo valido encontrada");
				return;
			}

			// Crear reader
			FileReader FReader = null;
			boolean error = false;
			try {
				FReader = new FileReader(ruta);

			} catch (FileNotFoundException exNoTrobat) {
				System.err.println("# Error, no se ha encontrado el fichero " + ruta);
				error = true;

			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}

			if (error) {
				try {
					FReader.close();
				} catch (Exception e) {
				}
				Funciones.Consola.enterParaSeguir();
				return;
			}

			try (BufferedReader br = new BufferedReader(FReader)) {

				String linea;
				boolean pausar = true;
				for (linea = br.readLine(); linea != null; linea = br.readLine()) {

					linea = linea.trim();

					if (linea.equals(""))
						continue;

					// Ver si tiene argumentos o formato especial
					switch (linea.charAt(0)) {

					case '#': // Comentario
						continue;

					case ';': // Pausar
						linea = linea.substring(1).trim(); // Quitar ";"
						Funciones.Consola.pausa(StringANumero(linea));
						continue;

					case '~': // Enter para continuar
						Funciones.Consola.enterParaSeguir();
						continue;

					case ':': // Sin pausa
						linea = linea.substring(1).trim(); // Quitar ":"
						pausar = false;
						break;

					case ',': // Dibujo => tabulado y sin pausa
						linea = "\t" + linea.substring(1); // Quitar ","
						pausar = false;
						break;

					case '-': // Linea vacia
						linea = "";
						pausar = false;
						break;

					}

					// Mirar comodines
					// %nombre% => Nombre personaje
					if (linea.contains("%nombre%")) {
						if (programa.Datos.PERSONAJE != null)
							linea = linea.replace("%nombre%", programa.Datos.PERSONAJE.nombre);
						else
							linea = linea.replace("%nombre%", "Personaje");
					}

					// Por defecto hace pausa de 2sec, a menos que indicemos lo contrario con ":"
					if (pausar)
						Funciones.Consola.pausa(Config.General.leer_tiempo_pausa);
					System.out.println(linea);

					pausar = true;
				}

			} catch (FileNotFoundException exNoTrobat) {
				System.err.println("# Error, no se ha encontrado el fichero " + ruta);
				exNoTrobat.printStackTrace();
			} catch (IOException exIO) {
				exIO.printStackTrace();

			} finally {

				// Tanca el File reader
				try {
					FReader.close();
				} catch (Exception e) {
				}

				// Pasi el que pasi, fes la pausa i demana Enter
				Funciones.Consola.pausa(2);
				Funciones.Consola.enterParaSeguir();
			}
		}

		// COJER FICHERO ALEATORIO
		public static String ficheroAleatorio(String ruta) {

			File[] listaArchivos = new File(ruta).listFiles(); // Ficheros en la ruta
			File fichero; // Fichero seleccionado
			int rand; // Index random

			// Al usar FOR y poner en nulo, probara todos los archivos
			// Tampoco quedara en bucle infinito probando (ahora tiene un maximo)
			for (int i = 0; i < listaArchivos.length; i++) {

				// Coje un fichero aleatorio
				rand = (int) (Math.random() * listaArchivos.length);
				fichero = listaArchivos[rand];

				// En caso de que sea nulo (ya se ha probado antes)
				while (fichero == null) {
					// Cojer otro aleatorio
					fichero = listaArchivos[(int) (Math.random() * listaArchivos.length)];
				}

				// Si es valido, devuelvelo
				if (fichero.isFile() && !fichero.getName().startsWith("_") && fichero.length() > 0 && fichero.canRead())
					return fichero.getAbsolutePath();
				else
					// Si es invalido, ponlo en null y sigue probando
					listaArchivos[rand] = null;
			}

			return null;

		}

		
//		// LINEA ALEATORIA DE UN FICHERO
//		public static String lineaFicheroAleatorio(String ruta) {
//			
//		}
		
	} // FIN CLASE - Datos

	////////////////////////////
	// CLASE - CONSOLA
	////////////////////////////
	public static class Consola {

		// LIMPIAR CONSOLA
		public static void clear() {
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		}

		// ESPERAR X SEGUNDOS
		public static void pausa(double segundos) {

			try {
				Thread.sleep((int) (segundos * 1000));
			} catch (Exception e) {
			}
		}

		// PRESIONA UNA TECLA PARA SEGUIR
		public static void enterParaSeguir() {

			System.out.println("\nPresiona Enter para continuar...");

			try {
				Joc.scn.nextLine();
			} catch (Exception e) {
			}
		}

		// PONER ESPACIOS A LA DERECHA
		public static String espaciadoDerecha(String texto, int num) {

			return String.format("%-" + num + "s", texto);

		}

	}

	//
	// ============= CONVERSIONES
	public static class Conversiones {
		// Horas a minutos
		public static int horasAMinutos(double horas) {
			return (int) Math.round(horas * 60);
		}

		// Minutos a horas
		public static double minutosAHoras(int minutos) {
			return minutos / 60d;
		}

		// Minutos a tiempo
		public static String minutosATiempo(int minutos) {

			int hora = minutos / 60;
			minutos = minutos % 60;

			return String.format("%d:%02d", hora, minutos);
		}
	}

	////////////////////////////
	// CLASE - INVENTARIO
	////////////////////////////
	public static class Inventario {

		// MOSTRAR INFO INVENTARIO
		private static void mostrarInfoInventario(programa.Inventario inv) {

			// INFORMACIO DE L'INVENTARI
			System.out.println("\n");
			System.out.println(" =========== " + inv.Nombre + " ==============");
			System.out.print(" ==== Capacidad: " + inv.Items.size() + "/" + inv.Capacidad); // Num items del total
			if (inv.Lleno())
				System.out.print(" [Lleno]");
			System.out.println();

			// //DISSENY 2
			// System.out.print(" =========== " + inv.Nombre + " [" + inv.Items.size() + "/"
			// + inv.Capacidad + "]");
			// System.out.print(" ==== Capacidad: " + inv.Items.size() + "/" +
			// inv.Capacidad); // Num items del total
			// if (inv.Lleno())
			// System.out.print(" [Lleno]");
			// System.out.println(" ==============");
		}

		// MOSTRAR ITEMS
		public static void mostrarInventario(programa.Inventario inv) {

			if (inv == null)
				return;

			boolean espaciado = true;
			String item;

			// INFORMACION DEL INVENTARIO
			mostrarInfoInventario(inv);

			// Mostra tots els espais de l'inventari, incluit els buits
			for (int i = 0; i < inv.Capacidad; i++) {

				// La posicio conte item?
				if (i < inv.Items.size())
					// Mostrar nombre item
					item = " [" + (i + 1) + "] " + inv.Items.get(i).nombre;
				else
					item = " [" + (i + 1) + "]";

				// Si va a la esquerra o a la dreta
				if (espaciado)
					// Item a la esquerra + espais
					System.out.print(Funciones.Consola.espaciadoDerecha(item, 22));
				else
					// Item a la dreta
					System.out.println(item);

				espaciado = !espaciado;

			}

		}

		// MOSTRAR ITEMS CON FILTRO
		public static void mostrarInventarioDetallado(programa.Inventario inv) {

			if (inv == null)
				return;

			boolean espaciado = true;
			programa.Items.Item item;
			String str;

			// INFORMACION DEL INVENTARIO
			mostrarInfoInventario(inv);

			// ITEMS DEL INVENTARIO
			// Mostrar que es la vista detallada
			System.out.println(" == > Detalles\n");

			// Mostra tots els espais de l'inventari, incluit els buits
			for (int i = 0; i < inv.Capacidad; i++) {

				// Mostra el index (ID) del item
				str = " [" + (i + 1) + "] ";

				// La posicio conte item?
				if (i < inv.Items.size()) {

					// Mostrar el nom del item
					str += inv.CojerItem(i).nombre;

					// Agafa item
					item = inv.CojerItem(i);

					// Mostra la informacio segons el tipus de item (la clase del item)
					// -- Comida
					if (item.getClass() == programa.Items.Comida.class) {

						// Muestra valor que recupera
						str += " (+" + Funciones.Items.aComida(item).Recupera + ")";

						// Muestra si esta cocinado
						if (Funciones.Items.aComida(item).Cocinado)
							str += "[C]";

					} else if (item.getClass() == programa.Items.Arma.class) {

						// -- Arma
						// Mostrar balas o cosas asi

					}

				} // else
					// // Mostra com a buit
					// str = " [" + (i + 1) + "]";

				// Si va a la esquerra o a la dreta
				if (espaciado)
					// Item a la esquerra + espais
					System.out.print(Funciones.Consola.espaciadoDerecha(str, 25));
				else
					// Item a la dreta
					System.out.println(str);

				espaciado = !espaciado;

			}

			if (!espaciado)
				System.out.println();
		}

		// MOSTRAR ITEMS CON FILTRO
		public static void mostrarInventarioFiltrado(programa.Inventario inv, Class<?> filtro_class) {

			if (inv == null)
				return;

			boolean espaciado = true;
			programa.Items.Item item;
			String str = "";

			// INFORMACION DEL INVENTARIO
			mostrarInfoInventario(inv);

			// ITEMS DEL INVENTARIO

			// Mostrar que s'ha filtrat
			System.out.println(" == " + filtro_class.getSimpleName() + " > Detalles\n");

			// Mostra tots els espais de l'inventari, incluit els buits
			for (int i = 0; i < inv.Capacidad; i++) {

				// La posicio conte item?
				if (i < inv.Items.size()) {

					// Segons la clase, mostra la informacio d'aquell tipus de item
					item = inv.CojerItem(i);

					// -- Comida
					if (item.getClass() == programa.Items.Comida.class && item.getClass() == filtro_class) {

						// Mostrar index (ID) i nombre del item
						str = " [" + (i + 1) + "] ";
						str += item.nombre;

						// Cuanto recupera?
						str += " (+" + Funciones.Items.aComida(item).Recupera + ")";

						// Cocinado? Muestralo
						if (Funciones.Items.aComida(item).Cocinado)
							str += "[C]";

					} else if (item.getClass() == programa.Items.Arma.class && item.getClass() == filtro_class) {

						// -- Arma

						// Mostrar index (ID) i nombre del item
						str = " " + (i + 1) + "-";
						str += inv.CojerItem(i).nombre;

						// Mostrar balas o cosas asi

					} else if (item.getClass() == filtro_class) {

						// Si es otro tipo
						// Mostrar index (ID) i nombre del item
						str = " " + (i + 1) + "-";
						str += inv.CojerItem(i).nombre;

					} else
						continue;

				} else
					// Si no hay item, no muestres nada
					continue;

				// Si va a la esquerra o a la dreta
				if (espaciado)
					// Item a la esquerra + espais
					System.out.print(Funciones.Consola.espaciadoDerecha(str, 22));
				else
					// Item a la dreta
					System.out.println(str);

				espaciado = !espaciado;

				str = "";
			}

		}

		// MOVER ITEM
		private static int moverItem(int posicion, programa.Inventario inv_items, programa.Inventario inv_recibe) {

			if (inv_recibe == null)
				return 0;

			int code;
			programa.Items.Item item;

			posicion--;

			// Cojer item a mover
			item = inv_items.CojerItem(posicion);

			// Si no encuentra el item, error
			if (item == null)
				return 404;

			// A�adir item al inventario receptor
			code = inv_recibe.A�adir(item);

			// Si ha dado algun error, devuelvelo
			if (code != 0)
				// 400 - Error al a�adir
				// 405 - Inventario lleno
				return code;

			// Quitar item del inventario emisor
			code = inv_items.QuitarIndex(posicion);

			// Devuelve el codigo final
			// 0 - Correcto
			// 400 - Error al quitar
			// 404 - No existe el item
			return code;

		}

		// INTERCAMBIAR ITEM
		private static int intercambiarItem(programa.Inventario inv_1, int posicion1, programa.Inventario inv_2,
				int posicion2) {

			// Guardar items temporalmente
			programa.Items.Item item1 = inv_1.CojerItem(posicion1 - 1);
			programa.Items.Item item2 = inv_2.CojerItem(posicion2 - 1);

			// Si existen
			if (item1 != null && item2 != null) {

				// Quitarlos
				inv_1.Quitar(item1);
				inv_2.Quitar(item2);

				// A�adirlos en la lista contraria (en la misma posicion)
				// inv_1.A�adir(item2);
				// inv_2.A�adir(item1);
				inv_1.Items.add(posicion1 - 1, item2);
				inv_2.Items.add(posicion2 - 1, item1);

				return 0;

			} else
				return 404;

		}

		// ADMINISTRAR INVENTARIO
		public static void administrarInventario(programa.Inventario inv_mio, programa.Inventario inv_otro, boolean fix) {

			Funciones.Consola.clear();
			if(fix)
				Joc.scn.nextLine();

			// Copiar inventarios a listas temporales
			programa.Inventario tmp_inv_mio = inv_mio;
			programa.Inventario tmp_inv_otro = inv_otro;

			// Leer comandos
			if (leerComandos(tmp_inv_mio, tmp_inv_otro)) {

				// Aplicar cambios de los inventarios temporales a los inventarios finales
				inv_mio = tmp_inv_mio;
				inv_otro = tmp_inv_otro;

			}
//			else {// Salir, no guardar cambios echos
//			}

			Funciones.Consola.clear();

		}

		// LEER COMANDOS
		private static boolean leerComandos(programa.Inventario inv_mio, programa.Inventario inv_otro) {

			boolean salir = false;
			boolean guardado = false;
			int accion = -1;

			do {

				// Si se ha echo alguna accion, lo muestra de nuevo
				if (accion != 0) {

					Funciones.Consola.clear();

					//Mostrar datos personaje
					System.out.println(Funciones.PersonajesF.informacion(programa.Datos.PERSONAJE));
					
					// Mostrar inventarios
					mostrarInventario(inv_mio);
					mostrarInventario(inv_otro);

					System.out.println("\nAccion:");
				}

				accion = 0;

				// Leer comandos
				String comando = Joc.scn.nextLine().toLowerCase().trim();

				// Separar la accion de los argumentos => accion itemID itemID
				String[] cmd = comando.split(" ");

				// Cojer las IDs
				ArrayList<Integer> args = Datos.cojerIDs(cmd, false);

				// Si no se han puesto IDs, vuelve arriba
				if (cmd.length < 1)
					continue;

				// Mira la accion
				switch (cmd[0]) {

				case "cojer": // [1] COJER
					accion = 1;
					break;

				case "dejar": // [2] DEJAR
				case "soltar":
					accion = 2;
					break;

				case "cambiar": // [3] INTERCAMBIAR
				case "intercambiar":
					// Si tiene suficientes itemIDs
					if (args.size() >= 2)
						accion = 3;
					break;

				case "comer": // [4] COMER
				case "usar":
					accion = 4;
					break;

//				case "romper":
//				case "destruir":

				case "ver": // [5] INVENTARIO DETALLADO/FILTRADO
					accion = 10;
					Funciones.Consola.clear();

					// Si no tiene argumento, muestra detallado
					if (cmd.length < 2) {
						Inventario.mostrarInventarioDetallado(inv_mio);
						Inventario.mostrarInventarioDetallado(inv_otro);

					} else {

						switch (cmd[1]) {
						case "comida":
						case "alimento":
						case "alimentos":
							Inventario.mostrarInventarioFiltrado(inv_mio, programa.Items.Comida.class);
							Inventario.mostrarInventarioFiltrado(inv_otro, programa.Items.Comida.class);
							break;

						case "armas":
						case "arma":
							Inventario.mostrarInventarioFiltrado(inv_mio, programa.Items.Arma.class);
							Inventario.mostrarInventarioFiltrado(inv_otro, programa.Items.Arma.class);
							break;

						default:
							Inventario.mostrarInventarioDetallado(inv_mio);
							Inventario.mostrarInventarioDetallado(inv_otro);
						}
					}
					System.out.println();
					Funciones.Consola.enterParaSeguir();
					break;

				case "aceptar": // [5] GUARDAR CAMBIOS
				case "guardar":
					guardado = true;
					accion = 11;
					break;

				case "finalizar":
				case "salir":
					accion = -1;
					// Si no se han guardado los intercambios, preguntar
					if (!guardado)
						if (Funciones.Datos.preguntar("Guardar los cambios?", false)) {
							guardado = true;
						}
					salir = true;
					break;

				case "cancelar":
					if (Funciones.Datos.preguntar("Seguro que quieres salir sin guardar?", false))
						return false;
				}

				// Hacer la accion (si es necessario)
				if (accion > 0 && accion < 10 && args.size() > 0) {

					guardado = false; // Se han echo cambios, marca como no guardado

					// NOTA: Mirar el codigo que devuelve
					procesar(accion, args, inv_mio, inv_otro);

					Funciones.Consola.enterParaSeguir();

				}

			} while (!salir);

			// Return:
			// True - Guardar cambios del inventario
			// False - No guardar cambios
			return guardado;

		}

		// PROCESAR COMANDO CON ARGUMENTOS
		private static void procesar(int accion, ArrayList<Integer> args, programa.Inventario inv_mio,
				programa.Inventario inv_otro) {

			// Si el otro inventario no existe y es intercambio, salir
			if (accion == 3 && inv_otro == null)
				return;

			int res = 0;
			boolean mas = false; // False => Pon item con frase, True => Adjunta item a la frase
			boolean error_lleno = false;

			// Si accion es "intercambiar", las IDs (elementos de "args") han de ir por
			// parejas, sino lo son se eliminara la ultima ID
			if (accion == 3 && args.size() % 2 != 0) {
				args.remove(args.size() - 1);

			} else if (accion != 3) {

				// Ordenas de mas grande a mas peque�o
				// Soluciona el re-ordenado de las listas al eliminar elementos
				// - Empieza por el ultimo y asi no se reordenan los de mas arriba
				// Ejemplo; Quitar 1,3,5: Si empezaramos quitando por el 1, el 3 y 5 cambiarian
				// de
				// indice
				Collections.sort(args, Collections.reverseOrder());

			}

			// Por cada argumento
			for (int i = 0; i < args.size(); i++) {

				// Si la ID es invalida (cojerIDs la retorna com a -1), continuar
				// Si es accion 3 (cambiar) van por parejas, saltar siguiente itemID y continuar
				if (args.get(i) == -1) {
					if (accion == 3)
						i++;
					continue;
				}

				res = 0;

				// Haz la accion correspondiente
				switch (accion) {

				case 1: // COJER
					// - Mover del otro inventario al jugador
					res = Funciones.Inventario.moverItem(args.get(i), inv_otro, inv_mio);

					// Si se cojio el item
					if (res == 0)
						if (!mas) {
							System.out.println("@ Has cojido " + inv_mio.CojerItemUltimo().nombre);
							mas = true;
						} else
							System.out.print(", " + inv_mio.CojerItemUltimo().nombre);// inv_mio.CojerItem(args.get(i)).nombre);
					break;

				case 2: // SOLTAR
					// - Mover del jugador al otro inventario
					res = Funciones.Inventario.moverItem(args.get(i), inv_mio, inv_otro);

					// Si se solto, muestralo
					if (res == 0)
						if (!mas) {
							System.out.print("@ Has dejado " + inv_otro.CojerItem(args.get(i) - 1).nombre);// inv_otro.CojerItemUltimo().nombre);
							mas = true;
						} else
							System.out.print(", " + inv_otro.CojerItem(args.get(i) - 1).nombre); // inv_mio.CojerItemUltimo().nombre);
					break;

				case 3: // INTERCAMBIO
					res = Funciones.Inventario.intercambiarItem(inv_mio, args.get(i), inv_otro, args.get(i + 1));
					// Si se cojio, muestralo
					if (res == 0)
						if (!mas) {
							System.out.print("@ Has cojido " + inv_mio.CojerItem(args.get(i) - 1).nombre); // CojerItemUltimo().nombre);
							mas = true;
						} else
							System.out.print(", " + inv_mio.CojerItem(args.get(i) - 1).nombre); // CojerItemUltimo().nombre);

					i++; // Saltar siguiente, van por parejas
					break;

				case 4: // COMER
					res = 0;
					// Cojer item
					programa.Items.Item item = inv_mio.CojerItem(args.get(i) - 1);

					// Si item es nulo, no lo ha encontrado. Salir con error
					if (item == null) {
						res = 404;
						break;
					}

					// Darlo de comer
					if (programa.Datos.PERSONAJE.comer(item))
						if (!mas) {
							System.out.print("@ Has comido " + item.nombre);
							mas = true;
						} else {
							System.out.print(", " + item.nombre);
						}
					else
						System.err.println("# La comida ya esta al maximo");
				}

				// Si ha dado algun fallo
				if (res != 0) {
					// Mira que fallo ha sido
					switch (res) {

					case 400: // Error al a�adir/quitar
						System.err.println("# Error al a�adir/quitar un item");
						break;

					case 404: // No existe el item
						if (args.get(i) != -1)
							System.err.println("# No existe ningun item con ID " + args.get(i));
						break;

					case 405: // Inventario lleno
						if (!error_lleno) {
							System.out.println("# El inventario esta lleno!");
							error_lleno = true;
						}
					}

				}

			}

		}

	} // FIN CLASE - Inventario

	////////////////////////////
	// CLASE - CASA
	////////////////////////////
	public static class CasaF {

		public static class Cocina {

			public static int CocinarItems(programa.Inventario inv) {

				boolean salir = false;
				String resp = "";
				programa.Items.Item item = null; // Cojer item solicitado
				programa.Items.Comida comida = null; //Transformar item a comida
				int num_cocinados = 0;

				do {

					Funciones.Consola.clear();

					Funciones.Inventario.mostrarInventarioFiltrado(inv, programa.Items.Comida.class);
					System.out.println(" [0] Salir\n");

					System.out.println("> Introduce los items a cocinar: ");

					// Leer linea con IDs
					resp = Joc.scn.nextLine().toLowerCase().trim();

					// Separar las IDs => itemID itemID
					String[] cmd = resp.split(" ");

					// Cojer las IDs, true porque el primer elemento tambien sera una ID y no un
					// comando
					ArrayList<Integer> IDs = Funciones.Datos.cojerIDs(cmd, true);

					System.out.println("\n");

					// Por cada itemID, cocinalo
					for (Integer id : IDs) {

						// Si quiere salir (indica un 0)
						if (id == 0) {
							if (Funciones.Datos.preguntar("> Quieres salir?", false))
								return num_cocinados++;
							else
								continue;
						}

						//Cojer item
						item = inv.CojerItem(id - 1);
						
						//Mirar si existe
						if (item == null) {
							System.err.println("# No existe el item con ID " + id);
							continue;
						}
						// Comprovar si es comida
						if (!item.tipo.equals("comida")) {
							System.err.println("# " + item.nombre + " no es comida!");
							continue;
						}

						// Coje el objeto del inventario y transformalo a Comida
						comida = Funciones.Items.aComida(item);

						// Intentar cocinarlo
						if (programa.Datos.CASA.Muebles.Cocina.Cocinar(comida)) {
							System.out
									.println("@ Has cocinado " + comida.nombre + " (Recupera " + comida.Recupera + ")");
							num_cocinados++;
						} else
							System.out.println("# " + comida.nombre + " ya estaba cocinado.");
					}

					Funciones.Consola.enterParaSeguir();

				} while (!salir);

				return num_cocinados;

			}

		}

	}

	////////////////////////////
	// CLASE - PERSONAJES
	////////////////////////////
	public static class PersonajesF {

		// VER NOMBRE
		public static String verNombre(programa.Personajes.Persona p) {
			if (p != null)
				return p.nombre;
			else
				return "-";
		}

		// CREAR PERSONAJE
		public static Personajes.Persona crearPersonaje() {

			Funciones.Consola.clear();
			// Joc.scn.nextLine();

			String nombre;

			// Variables para el personaje
			Personajes.Persona p = new Personajes.Persona(null);

			do {

				// ---------- Menu
				System.out.println(".===== Crear personaje ======.");
				// System.out.println("|============================|");

				System.out.print("- Nombre: ");
				nombre = Joc.scn.nextLine();

				System.out.println();
				while (!Funciones.PersonajesF.validarNombre(nombre)) {

					System.out.print("\n- Nombre: ");
					nombre = Joc.scn.nextLine();
					System.out.println();
				}

				Funciones.Consola.clear();

				// Mostrar resumen
				System.out.println(" .======= Personaje =======.");
				System.out.println(" | Nombre: " + nombre);
				System.out.println(" �=========================�");

			} while (!Funciones.Datos.preguntar("\nGuardar y continuar?", false));

			// Asignar valores
			p.nombre = nombre;
			p.Inventario.Nombre = nombre;

			// Retorna el personaje
			return p;

		}

		// MODIFICAR PERSONAJE
		public static Personajes.Persona modificarPersonaje(Personajes.Persona p) {

			Personajes.Persona original = p; // Copia del original
			boolean salir = false;

			// Modificar valores del personaje
			do {

				// Mostrar menu de creacion del personaje
				switch (Menus.MenuModificarPersonaje(p)) {

				case 1:
					while (!Funciones.PersonajesF.validarNombre(p.nombre))
						p.nombre = Funciones.Datos.cambiarValor(p.nombre, true);
					break;

				case -1: // Salir sin guardar el personaje
					// Devolver original, sin modificar
					return original;

				case 0:
					salir = true;

				}

			} while (!salir);

			return p;

		}

		// VALIDAR NOMBRE
		public static boolean validarNombre(String nombre) {

			// NOTA: Separar y que muestre diferentes mensajes de error?

			if (nombre == null || nombre.equals("") || nombre.length() > 15) {
				System.err.println("# El nombre ha de tener entre 1 y 15 caracteres");
				return false;
			} else
				return true;
		}

		// COMPROVAR PERSONAJE ES VALIDO
		public static boolean validarPersonaje(Personajes.Persona p) {

			// Pregunta hasta que el usuario decida:
			// 1. Crear el personaje otra vez
			// 2. No crear el personaje y salir al menu
			while (p == null) {

				System.out.println("Atencion: El personaje no ha sido creado!");

				if (Funciones.Datos.preguntar("Quieres volver a crearlo?", true)) {

					// Quiere hacer el personaje, mostrar menu
					p = Funciones.PersonajesF.crearPersonaje();

				} else {

					// No quiere hacer personaje, salir al menu principal
					return false;

				}
			}
			return true;
		}

		// CALCULAR HAMBRE
		public static boolean calcularStatsGastados(Personajes.Persona p, int minutos) {

			if (p.comida > 50) // Si tiene mas del 50%, que recupere vida
				p.Acciones.CambiarVida(minutos * Config.Personajes.vida_por_minuto);

			// Calcular segun los minutos usados
			p.Acciones.CambiarEnergia(minutos * Config.Personajes.energia_por_minuto);

			// Calcular segun los minutos usados
			if (!p.Acciones.CambiarComida(minutos * Config.Personajes.comida_por_minuto))
				return false;

			return true;

		}

		// VER INFORMACION
		public static String informacion(Personajes.Persona p) {

			if (p != null) {

				String info = "";

				info += " Nombre: " + p.nombre + "\n";
				info += "  Salud: " + Funciones.Datos.graficoSegunValor(p.vida, 100);
				info += " Comida: " + Funciones.Datos.graficoSegunValor(p.comida, 100);
				info += "Energia: " + Funciones.Datos.graficoSegunValor(p.energia, 100);

				return info;

			} else
				return "# Error: El personaje es nulo";
		}

	}// FIN CLASE - Personajes

	////////////////////////////
	// CLASE - ITEMS
	////////////////////////////
	public static class Items {

		// CONVERTIR OBJETO A SU TIPO
		public static programa.Items.Comida aComida(programa.Items.Item obj) {
			return (programa.Items.Comida) obj;
		}

		// CONVERTIR A ARMA
		public static programa.Items.Arma aArma(programa.Items.Item obj) {
			return (programa.Items.Arma) obj;
		}

	}

	public static class Juego {

		public static void ResumenPartida(int dias) {

			Funciones.Consola.clear();

			programa.Personajes.Persona p = programa.Datos.PERSONAJE;

			System.out.println("         _____________\n" + "        |_ __/ /\\ __ _|\n" + "        |_ GAME OVER _|\n"
					+ "        |__/_/__\\_\\___|\n" + "\n\n\n");

			Funciones.Consola.pausa(4);
			Funciones.Consola.enterParaSeguir();
			Funciones.Consola.clear();

			// System.out.println("####### Fin del juego #######");
			// System.out.println();

			System.out.println("=============================");
			System.out.println("=== Resumen de la partida ===");
			System.out.println("-----------------------------");

			System.out.println(" Dias sobrevividos: " + dias);

			Funciones.PersonajesF.informacion(p);
			System.out.println();

			System.out.print(" Causa de la muerte: ");
			if (p.comida == 0)
				System.out.println("Hambre");
			else
				System.out.println("Heridas");

			System.out.println();
			if (programa.Config.Eventos.perro_encontrado)
				System.out.println("[Adicional] Has encontrado al perro");

//			// PROXIMO: Guardar resumen en un archivo
//			if(Funciones.Datos.preguntar("> Guardar resumen?", true)) {
//				//Escribir a un archivo
//			}

			Funciones.Consola.enterParaSeguir();

			Funciones.Consola.clear();
		}

	}
} // FIN
