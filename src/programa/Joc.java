package programa;

import java.util.Scanner;

public class Joc {

	static Scanner scn = new Scanner(System.in);

	public static void main(String[] args) {

		boolean sortir = false;

		do {

			switch (Menus.MenuPrincipal()) {

			case 1:
				joc();
				break;

			case 2:
				break;

			case 0:
				sortir = true;
				break;

			default:
				System.out.println("Elije una opcion valida!");

			}

		} while (!sortir);

		// Missatge de sortida
		Funciones.Consola.clear();
		System.out.println();
		System.out.println("\t .===================.");
		System.out.println("\t | Gracias por jugar |");
		System.out.println("\t �===================�");
		System.out.println("\n\n");

	}

	// ----------------------------------------------------------------------

	// JOC
	public static void joc() {

		Funciones.Consola.clear();
		scn.nextLine();

		// Crear personaje
		Datos.PERSONAJE = Funciones.PersonajesF.crearPersonaje();

		// Mostrar introduccion/historia
		Funciones.Historia.IntroduccionAleatoria();

		// Si no se quiere crear personaje
		if (!Funciones.PersonajesF.validarPersonaje(Datos.PERSONAJE))
			return; // Salir al menu principal

		// Cargar todos los datos del juego
		Datos.Cargar();

		// Empezar juego
		jugarPartida(Datos.PERSONAJE);

	}

	// JUGAR PARTIDA
	public static void jugarPartida(Personajes.Persona p) {

		// TEST ---------------------
		p.vida = 5;
		p.comida = 0;
		p.energia = 5;
		// ---------------------------

//		//El rescate ahora se calcula solo en la clase "Rescate"
//		int dia_rescate = 30 + (int) (Math.random() * 50);
		// Dias hasta ser rescatados -- Minimo 30 dias => total = 30 + dias aleatorios
		int dia = 1; // Numero de dias que sobrevive
		int res_dia = 0; // Codigo que devolvera el dia
		int min_sobrantes = 0; // Numero de minutos que no se pudieron gastar el dia anterior, se gastaran el
								// siguiente

		do {

			// Si es el dia del rescate
			if (dia == Rescate.Dia) {

				// Ser rescatado o quedarse??
				if (Rescate.preguntarRescate())
					// Ser rescatado
					// - Mostrar resumen
					// resumenPartida();
					// - Salir al menu
					return;
				else
					// A�adir mas dias hasta el proximo rescate
					Rescate.prolongarRescate();

			} else {

				// Ver si hay evento aleatorio
				Eventos.eventoAleatorio(p, dia);

				// Comenzar dia
				res_dia = comenzarDia(p, dia, min_sobrantes);

				// Segun lo que haya pasado ese dia
				if (res_dia == -1) { // Salir del juego
					return;

				} else if (res_dia == -2) { // Personaje ha muerto
					Funciones.Juego.ResumenPartida(dia);
					return;

				} else { // Todo correcto, guardar minutos gastados que no cabian en ese dia
					min_sobrantes = res_dia;
				}

			}

			dia++;

		} while (res_dia >= 0); // && dia <= Rescate.Dia);

	}

	// DIA
	public static int comenzarDia(Personajes.Persona p, int dia, int minutos_anteriores) {

		int minutos = 0; // Minutos que han pasado en este dia
		int mins_usados = 0; // Tiempo usado en la accion seleccionada

		// Sumar las horas que no cabian en el dia anterior
		minutos += minutos_anteriores;

		// Nota: Utilitzar un while, si el dia anterior se uso una accion que dura mas
		// de un dia <=> 24h <=> 1440min, tendra que saltar dias hasta llegar donde
		// deberia

		// Por cada dia / ciclo de 1440min = 24h
		while (mins_usados >= 0 && minutos < 1440) {

			// Quitar comida
			// Quitar energia

			// Si esta explorando o no
			if (p.explorando) {

				// Muestra menu de exploracion y coje las horas usadas que devuelva
				mins_usados = Menus.Explorar.menuPrincipal(p, dia, minutos);

			} else {

				// Muestra el menu de la casa y coje las horas usadas que devuelva
				mins_usados = Menus.Casa.menuPrincipal(p, dia, minutos);
			}

			// Mirar si el usuario queria salir del programa
			if (mins_usados == -1)
				return -1;

			else if (mins_usados == -2) // Si ha muerto
				return -2;

			else if (mins_usados > 0) {

				// Calcular stats gastados
				if (!Funciones.PersonajesF.calcularStatsGastados(p, mins_usados))
					return -2; // Ha muerto (por hambre)

				// Suma las horas al total diario
				minutos += mins_usados;
			}

		}

		// Calcula los minutos que sobran
		mins_usados = minutos - 1440;
		return mins_usados;

		// Retorna:
		// -1 => Salir al menu principal
		// -2 => Personaje ha muerto
		// >= 0 => Todo correcto, devuelve minutos sobrantes

	}

}
