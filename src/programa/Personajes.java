package programa;

public class Personajes {

	public static class PersonajeBase {

		String nombre;
		public int vida = 100;
		public double energia = 100;
		public double comida = 100;

		// Constructor de la clase
		public PersonajeBase(String nombre) {
			this.nombre = nombre;
		}

		// SUB-CLASE ACCIONES
		public class pAcciones {

			// COMER
			private Items.Item comerItem(Items.Item item) {
				
				//Si puede comer, comera y devolvera el item para quitarlo del inventario
				// Si ya esta lleno, no comera
				if(CambiarComida(Funciones.Items.aComida(item).Recupera))
					return item;
				else
					return null;
				

			}

			// QUITAR COMIDA
			public boolean CambiarComida(double _comida) {

				// Si se quiere sumar (>0) y ya tiene el maximo (100)
				if (_comida > 0 && comida == 100)
					return false;

				// Suma/resta la energia
				comida += _comida;

				// Comprueva que no salga de los limites
				if (comida > 100)
					comida = 100;

				else if (comida < 0) {

					// Si comida esta en negativo, sumalo a la vida (restara al ser negativo)
					if (!CambiarVida(comida))
						return false;

					comida = 0;
				}

				return true;
			}

			// MODIFICAR ENERGIA
			public boolean CambiarEnergia(double _energia) {

				// Si se quiere sumar y ya tiene el maximo O si quiere restar y ya tiene el
				// minimo
				if (_energia > 0 && energia == 100 || _energia < 0 && energia == 0)
					return false;

				// Suma/resta la energia
				energia += _energia;

				// Comprueva que no salga de los limites
				if (energia > 100)
					energia = 100;
				else if (energia < 0)
					energia = 0;

				return true;
			}

			// MODIFICAR VIDA
			public boolean CambiarVida(double _vida) {

				if (_vida > 0 && vida == 100)
					return true;

				// Suma/resta la energia
				vida += _vida;

				// Comprueva que no muera
				if (vida > 100)
					vida = 100;

				else if (vida <= 0) {
					vida = 0;
					return false;
				}
				return true;

			}

			// MODIFICAR NOMBRE
			public boolean renombrar(String nuevo_nombre) {

				if (Funciones.PersonajesF.validarNombre(nuevo_nombre)) {
					nombre = nuevo_nombre;
					return true;
				} else
					return false;
			}

		}

	}

	// PERSONA
	public static class Persona extends PersonajeBase {

		boolean explorando = false;

		// Constructor de persona
		public Persona(String _nombre) {
			super(_nombre); // Llama al constructor de la clase base
			Inventario = new Inventario(nombre, 4);
		}

		private Animal mascota = null;
		
		// - Acciones
		pAcciones Acciones = new pAcciones();
		// - Inventario
		Inventario Inventario; // = new Inventario("Inventario " + nombre, 4);

		//COMIDA
		public boolean comer(Items.Item item) {
			
			if(item == null)
				return false;
			
			item = Acciones.comerItem(item);
			if(item != null) {
				Inventario.Quitar(item);
				return true;
			} else
				return false;
		}
		
		
		//MASCOTA
		public void assignarMascota(Animal mascota) {
			this.mascota = mascota;
		}
		public void dejarMascota() {
			this.mascota = null;
		}
		public Animal verMascota() {
			return mascota;
		}
		
	}

	// PERRO
	public static class Animal extends PersonajeBase {

		public String tipo = "animal";

		// Constructor de perro
		public Animal(String nombre, String tipo) {
			super(nombre);
			this.tipo = tipo.toLowerCase().trim();
		}

		// CAMBIAR NOMBRE
		public boolean cambiarNombre(String nuevo_nombre) {
			nombre = nuevo_nombre.trim();
			return true;
		}
	}

}
