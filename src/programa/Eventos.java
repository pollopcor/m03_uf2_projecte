package programa;

public class Eventos {

	// public static class EventoBase {
	//
	// String Nombre = "";
	//
	// public EventoBase(String _nombre) {
	// Nombre = _nombre;
	// }
	//
	// Inventario Inventario = new Inventario("Evento", 5);
	//
	// }
	// ArrayList<Eventos> Lista = new ArrayList<Evento>

	// --------- FUNCIONES ----------------------
	// EVENTO AL EMPEZAR EL DIA
	public static void eventoAleatorio(Personajes.Persona p, int dia) {

		// Si esta explorando, tendra mas posibildades de encontrar eventos
		if (p.explorando) {

			Funciones.Consola.clear();

			// Se determina si habra un evento mientras explora
			if (habraEvento(2)) {

				// Selecciona un evento aleatorio (con probabilidades)
				Eventos.eventoAleatorioExplorar(p, dia);

			} else {

				// MEJORA: Leer frase aleatoria de un fichero con este tipo de frases
				switch ((int) (Math.random() * 5)) {
				case 1:
					System.out.println("@ No has encontrado nada util");
					break;
				case 2:
					System.out.println("@ Parece que esa casa no tenia nada");
					break;
				case 3:
					System.out.println("@ Buscas en un coche... esta completamente vacio");
					break;
				case 4:
					System.out.println("@ Hay una lata de comida, desafortunadamente esta mala y tiene bichos");
					break;
				case 5:
					System.out.println("@ Entras a una casa, la cocina no tiene nada de comida");
					break;
				default:
					System.out.println("@ No vistes nada util por el camino");
					break;
				}
				Funciones.Consola.enterParaSeguir();
			}

		} else {

			// Se determina si habra un evento en la casa
			if (habraEvento(1))
				// Selecciona un evento aleatorio (con probabilidades)
				Eventos.eventoAleatorioCasa(p, dia);
		}
	}

	private static boolean habraEvento(int tipo) {

		boolean si = false;

		if (tipo == 1) { // EVENTO DE CASA

			if ((int) (Math.random() * 4 + 1) == 2 && Config.Eventos.dias_ultimo_evento > 3)
				si = true;

		} else if (tipo == 2) { // EVENTO DE EXPLORAR

			if ((int) (Math.random() * 3 + 1) == 2)
				si = true;
		}

		// Si habra evento
		if (si) {
			Config.Eventos.dias_ultimo_evento = 0;
			return true;
		} else {
			Config.Eventos.dias_ultimo_evento++;
			return false;
		}

	}

	// ===============================================================

	// DE LOS EVENTOS, HACER PROBABILIDADES
	private static void eventoAleatorioCasa(Personajes.Persona p, int dia) {

		// https://stackoverflow.com/questions/20327958/random-number-with-probabilities/20328412#20328412

		int r = (int) (Math.random() * 100);

		// Probabilidades de los eventos
		if (r > 85) {
			// ataque zombie, 15% probabilidad
			Evento.AtaqueZombieCasa();

		} else if (r > 70) {
			// llaman a la puerta, 10% probabilidad del total
			Evento.LlamanCasa();

		} else if (r > 50 && !Config.Eventos.perro_encontrado) {
			// Perro, 20% probabilidad del total
			Evento.Perro();

		} else if (r > 0) {
			// Encuentras un item, 50%

			// Si hay miembros, ellos encuentran el item, sino eres tu
			if (Datos.GRUPO.Miembros.size() > 0) {
				// Alguien encuentra un item
				Evento.MiembroEncuentraItem();

			} else {
				// Tu encuentras un item
				Evento.PersonajeEncuentraItem();

			}
		}

	}

	private static void eventoAleatorioExplorar(Personajes.Persona p, int dia) {

		int r = (int) (Math.random() * 100);

		// Probabilidades de los eventos
		if (r > 85) {
			// Encontrar persona, 15% probabilidad
			Evento.AtaqueZombieCasa();

		} else if (r > 65 && !Config.Eventos.perro_encontrado) {
			// Perro, 35% probabilidad del total
			Evento.Perro();

		} else if (r > 0) {

			// Item aleatorio (con probabilidades), 50% del total
			Evento.PersonajeEncuentraItem();

		}

	}

	// LISTA EVENTOS
	public static class Evento {

		// ATAQUE ZOMBIE
		public static void AtaqueZombieCasa() {

		}

		// ----------- PERRO
		public static void Perro() {

			boolean salir = false;

			// Si esta explorando
			if (Datos.PERSONAJE.explorando) {

				// El sitio puede ser aleatorio, de una lista de sitios
				System.out.println(
						"Mientras buscas sitios de interes y objetos utils por los alrededores, te percatas de que hay una casa con la puerta medio abierta.");
				System.out.println(
						"Es un objetivo facil de donde podrias conseguir cosas que necesitas como comida o materiales, incluso algun arma.");
				System.out.println(
						"Por otro lado, si la puerta esta abierta es por algun motivo y podria esconderse algun peligro dentro de la casa.");

				// ENTRAR CASA?
				// Parte 1, SI
				if (Funciones.Datos.preguntar("> Entrar a la casa?", false)) {

					System.out.println("Decides entrar a la casa con cautela pues no sabes lo que encontraras.");
					Funciones.Consola.pausa(2);
					System.out.println(
							"Miras alrededor y no ves a nadie. Decides ponerte manos a la obra y buscar algo util.");
					System.out.println("* Rebuscas entre los muebles *");
					Funciones.Consola.pausa(5);
					System.out.println("Escuchas unos ruidos extra�os que provienen del piso superior.");
					Funciones.Consola.pausa(2);
					System.out.print("Te quedas en silencio esperando reconocerlo con exactitud.");
					System.out.println(
							"Sientes como la cosa que se esconde ahi tambien se ha quedado en silencio, quizas haya notado tu presencia.");

					// SEGUIR BUSCANDO ?
					if (!Funciones.Datos.preguntar("Quieres dejar de buscar?", false)) {
						Funciones.Consola.pausa(2);
						// Dar item random
						System.out.println("Has encontrado ItemRandom()");
					}

					Funciones.Consola.pausa(3);

					System.out.println(
							"Escuchas otro ruido, parece que esta andado por el piso superior de forma extra�a, notas como se dirije a las escaleras.");
					System.out.println(
							"Un escalofrio recorre tu cuerpo, podrias estar poniendote en peligro estando alli");

					// INVESTIGAR RUIDO?
					// Parte 2, SI
					if (Funciones.Datos.preguntar("Ir hacia las escaleras para investigar?", false)) {

						System.out.println(
								"Avanzas con cautela hacia el principio de las escaleras y te quedas mirando a la oscuridad del piso superior");
						System.out.println("Escuchas como los pasos se acercan, calculas cuanto tardara en asomar...");
						Funciones.Consola.pausa(1);
						System.out.print("5.. ");
						Funciones.Consola.pausa(1);
						System.out.print("4.. ");
						Funciones.Consola.pausa(1);
						// System.out.print("*se te acelera el corazon* ");
						System.out.println("*te augmentan las pulsaciones* ");
						Funciones.Consola.pausa(1);
						System.out.print("3.. ");
						Funciones.Consola.pausa(1);
						System.out.println("2.. ");

						// HUIR?
						// Parte 3, NO
						if (!Funciones.Datos.preguntar("> HUIR!!", false)) {

							System.out
									.println("Respiras hondo y mantienes la mirada firme hacia la oscuridad de arriba");
							System.out.println(
									"Una pata peluda asoma por el final y notas dos ojos furtivos mirandote fijamente");
							Funciones.Consola.pausa(1);
							System.out.println("Esa figura te parece muy familiar... quizas demasiado.");
							System.out.println("El perro te ladra desde encima de las escaleras\n"
									+ "Tu intentas calmarlo y le extiendes la mano como se�al de tranquilidad\n"
									+ "El animal baja por las escaleras moviendo la cola y se acerca para lamerte\n");

						} else { // Parte 3, SI

							System.out.println(
									"Notas tu corazon palpitando como si fuera a salirse del pecho. Tus piernas reaccionan a la situacion y sales corriendo de la casa pegando un portazo.");
							salir = true;

						}
					} else { // Parte 2, NO

						System.out.println(
								"Decides salir de alli, tu vida es lo suficientemente valiosa como para no correr riesgos inecesarios.");

					}

				} else { // Parte 1, NO

					System.out.println(
							"Es mas valiosa tu vida que los objetos de esa casa. Eres precavido y prefieres no correr riesgos inecesarios.");
					salir = true;
				}

			} else { // EVENTO SI ESTA EN CASA

				System.out.println("Das una vuelta por la casa, hoy hace un buen dia como observas por la ventana.");
				System.out.println(
						"Tienes una terraza con unas vistas cuanto menos interesantes y parece buena idea ir a pasar un rato alli");

				// Parte 1, SI
				if (Funciones.Datos.preguntar("Subir a la terraza?", false)) {

					System.out.println("Decides subir a ver como va el mundo exterior ...o lo que queda de el");
					System.out.println("Desde alli se ve la calle delantera y algunas de las casas de alrededor");
					System.out.println(
							"Las vistas han empeorado bastante desde que las personas ya no son personas del todo");
					System.out.println("Te quedas mirando los edificios");
					Funciones.Consola.pausa(4);
					System.out.println("Escuchas unos ruidos extra�os");
					Funciones.Consola.pausa(2);
					System.out.println("Parece que provienen de unos contenedores al otro lado de la calle.");
					System.out.println("Te asomas a la barandilla y tus manos hacen un ligero ruido al cojerte a ella");

					System.out.println("Te quedas en silencio esperando reconocer ese ruido con exactitud.");
					System.out.println(
							"Sientes como la cosa que se esconde ahi tambien se ha quedado en silencio, quizas haya notado tu presencia.");

					// Parte 2, NO
					if (!Funciones.Datos.preguntar("> Quieres ir a echar un vistazo?", false)) {
						System.out.println("Salir a la calle es arrisgarse y m�s sabiendo que hay algo al otro lado");
						System.out.println("Decides quedarte alli mirando");
						salir = true;

					} else {// Parte 2, SI

						System.out.println("Decides ir a echar un vistazo y ver que clase de bicho se esconde alli.");
						// Mejora: Si el personaje tiene arma, que diga: Cojer tu *nombreArma*
						System.out.println("Sales a la calle cerrando la puerta tras de ti y cruzas con cautela");
						Funciones.Consola.pausa(3);
						System.out.println(
								"Estas al lado de los contenedores, se escucha un movimiento detras de un armario roto alli tirado.");
						System.out.println("Sabes que podria ser peligroso");

						// Parte 3, NO
						if (!Funciones.Datos.preguntar("> Alejarse y volver a casa?", false)) {

							System.out.println(
									"Cruzas al lado del primer contenedor y asomas tu cabeza entre el armario y el segundo contenedor");
							Funciones.Consola.pausa(3);
							System.out.println(
									"Ves dos patas y una cabeza peluda con las orejas subidas y dos ojos furtivos mirandote fijamente");
							System.out.println("Esa cosa te es demasiado familiar.");
							System.out.println(
									"El perro te ladra. Tu intentas calmarlo y le extiendes la mano como se�al de no amenaza");
							Funciones.Consola.pausa(3);
							System.out.println(
									"El animal baja las orejas y empieza a mover la cola mientras te lame la mano");

							System.out.println(
									":: Los perros tienen buen oido y olfato, quizas te seria util quedartelo y asi tendrias un nuevo amigo ::");

							if (Funciones.Datos.preguntar("> Llevartelo a casa?", false)) { // Parte 4, si

								System.out.println("Le silbas y te diriges a casa, el perro te sigue tranquilamente");

								// A�adir perro y marcar como obtenido
								Datos.MASCOTAS.a�adirMascota(Datos.Aleatorio.Animal("perro"));
								Config.Eventos.perro_encontrado = true;

								System.out.println("**************************");
								System.out.println(
										"@ Has conseguido un perro, te sera de ayuda para encontrar m�s items utiles (como comida) mientras exploras");
								Funciones.Consola.enterParaSeguir();
								return;

							} else {// Parte 4, NO

								System.out
										.println("Te alejas del perro y vuelves a casa, te las puedes apa�ar tu solo.");
								salir = true;
							}

						} else { // Parte 3, NO

							System.out
									.println("Decides volver a paso ligero hacia tu casa, vigilando que nada te siga");
							salir = true;

						}

					}

				} else {

					System.out.println(
							"Prefieres quedarte con la tranquilidad que te proporciona el interior de la casa");
					salir = true;
				}

			}

			// Si salio antes de tiempo
			if (salir) {
				Funciones.Consola.pausa(1);
				System.out.println("Otro dia m�s con vida");
				if (Datos.PERSONAJE.explorando) {
					System.out.println("@ Prosigues tu camino");
				} else
					System.out.println("@ Das una vuelta por la casa");

				Funciones.Consola.pausa(3);
			}

		}

		// PERSONA LLAMA
		public static void LlamanCasa() {

			System.out.println("<# No disponible #>");

			return;

			// System.out.println("Llaman al timbre de la casa. Extra�ado te asomas a la
			// ventana");

		}

		// PERSONAJE ENCUENTRA ITEM
		public static void PersonajeEncuentraItem() {

			System.out.print("@ Has encontrado ");
			EncuentraItem();

		}

		// MIEMBRO GRUPO ENCUENTRA ITEM
		public static void MiembroEncuentraItem() {

			System.out.print("@ Un miembro de tu grupo ha estado rebuscando por la zona y ha encontrado ");
			EncuentraItem();
		}

		private static void EncuentraItem() { // boolean permitir_casa) {

			// Cojer item aleatorio
			Items.Item item = Datos.Aleatorio.Item();

			System.out.println(item.nombre);

			if (Funciones.Datos.preguntar("\nQuieres guardarlo?", false)) {

				// Si tu inventario esta lleno
				if (Datos.PERSONAJE.Inventario.Lleno()) {

					if (Funciones.Datos.preguntar("Tu inventario esta lleno, intercambiar por otro item?", false)) {

						// Crear inventario temporal
						Inventario tmp_inv = new Inventario("Item encontrado", 1);
						tmp_inv.A�adir(item);

						// Intercambiar con ese inventario
						Funciones.Inventario.administrarInventario(Datos.PERSONAJE.Inventario, tmp_inv, false);
					}

				} else {

					Datos.PERSONAJE.Inventario.A�adir(item);
					System.out.println("@ Se ha guardado en tu inventario.");
				}
			}
		}

	}// FIN CLASE - Evento

}
