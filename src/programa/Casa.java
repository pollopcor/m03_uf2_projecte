package programa;

public class Casa {

	// - Inventario
	public Inventario Inventario = new Inventario("Casa", 10);

	public Muebles Muebles = new Muebles();
	
	public class Muebles {

		//Crear cama y cocina
		Cocina Cocina = new Cocina();
		Cama Cama = new Cama();
		
		// SUB-CLASE CAMA
		public class Cama {

			int recupera = 5;
			int nivel = 1;

			// CAMA - DORMIR
			public boolean dormir(Personajes.Persona p) {

				// Hacer que augmente la energia segun el valor base * nivel de la cama
				return p.Acciones.CambiarEnergia(recupera * nivel);
			}

			
			
//			//SUBIR NIVEL
//			public boolean mejorar() {
//				
//				//Si el inventario contiene X cantidad de X item, subir de nivel
//				return true;
//			}

		}

		// SUB-CLASE FOGONES
		public class Cocina {

			// COCINA - COCINAR
			public boolean Cocinar(Items.Comida comida) {

				if (!comida.Cocinado) {

					// Augmenta el valor de comida que recupera
					comida.Recupera *= 1.20;
					comida.Cocinado = true;
					return true;
					
				} else
					return false;
			}

			
		} // FIN CLASE - Fogones
	}

	// CLASE - Barricadas
	public class Barricadas {

		public double estado = 100;
		public int nivel = 0;
		public int resistencia = 50 * nivel;

		// MEJORAR
		public boolean mejorar() {

			// SI el inventario contiene X cantidad de X item, sube nivel
			return true;
		}

		// REPARAR

	} // FIN CLASE - Barricadas

}
