package programa;

import java.util.ArrayList;

public class Inventario {

	String Nombre = "Inventario";
	
	int Capacidad = 4; // Capacidad
	
	ArrayList<Items.Item> Items = new ArrayList<Items.Item>(); // Items del inventario

	//CONSTRUCTOR
	public Inventario (String nombre, int capacidad) {
		this.Nombre = nombre;
		this.Capacidad = capacidad;
	}

	// -----------------------------
	// FUNCIONES
	// --------------------------------

	// COMPROVAR LLENO
	public boolean Lleno() {
		if (Items.size() >= Capacidad)
			return true;
		else
			return false;
	}

	// A�ADIR ITEM
	public int A�adir(Items.Item item) {

		// Esta lleno?
		if (Lleno())
			return 405;

		// A�adir
		if (Items.add(item))
			return 0;
		else
			return 400;

		// Return:
		// 0 - Completado
		// 400 - No se ha a�adido
		// 405 - Esta lleno

	}

	// QUITAR ITEM
	public boolean Quitar(Items.Item item) {

		return Items.remove(item);

		// Return:
		// True - Correcto
		// False - No existe el item
	}

	// QUITAR ITEM POR POSICION
	public int QuitarIndex(int posicion) {

		try {
			Items.remove(posicion);
			return 0;

		} catch (IndexOutOfBoundsException error) {
			// Error, posicion se sale (el item no existira)
			return 404;

		} catch (Exception e) {
			// Error general
			return 400;
		}

		// Return:
		// 0 - Correcto
		// 400 - Error al eliminar
		// 404 - No existe el item
	}

	// COJER ITEM
	public Items.Item CojerItem(int posicion) {

		try {
			return Items.get(posicion);

		} catch (IndexOutOfBoundsException error) {
			// Error, posicion se sale (el item no existira)
			return null;

		}

		// Return
		// Item - Correcto
		// Null - No hay item en la posicion
	}

	//COJER ULTIMO ITEM
	public Items.Item CojerItemUltimo() {
		return Items.get(Items.size() - 1);
	}
	
	//COJER POR TIPO
	public ArrayList<Items.Item> cojerTipo(String tipo) {
		
		ArrayList<Items.Item> tmp_items = new ArrayList<Items.Item>();
		
		for(int i=0;i<Capacidad;i++) {
			
			//Cojer todos los que sean del mismo tipo
			if(Items.get(i).tipo.equals(tipo))
				tmp_items.add(Items.get(i));
		}
		
		if(tmp_items.size() == 0)
			return null;
		else
			return tmp_items;
	}
	
}
