package programa;

import java.util.ArrayList;

public class Grupo {

	ArrayList<Personajes.Persona> Miembros = new ArrayList<Personajes.Persona>();

//		//CONSTRUCTOR
//		public Grupo() {
//		
//		}

	// AŅADIR MIEMBRO
	public void aņadirMiembro(Personajes.Persona miembro) {
		Miembros.add(miembro);
	}

	// QUITAR MIEMBRO
	public void quitarMiembro(Personajes.Persona miembro) {
		Miembros.remove(miembro);
	}

	// QUITAR MIEMBRO POR INDICE
	public int quitarMiembroIndice(int posicion) {

		try {
			Miembros.remove(posicion);
			return 0;

		} catch (IndexOutOfBoundsException error) { // Posicion sale de la lista, no existe
			return 404;

		} catch (Exception e) {
			return 400;
		} // Error general
	}

}
