package programa;

public class Config {

	// DATOS
	public static class General {

		// Pausa (en segundos) al mostrar cada linea de un fichero de historia
		public static final double leer_tiempo_pausa = 3;
		
	}

	// Juego
	public static class Eventos {

		//Esto es informacion/datos
		//Mover a Datos?
		
		public static int dias_ultimo_evento = 0; // Dias desde el ultimo evento

		public static boolean perro_encontrado = false; // Ya tiene al perro
		public static int ultimo_ataque = 0; // Dias desde ultimo ataque

	}

	// PERSONAJES
	public static class Personajes {

		// Comida y energia consumida por minuto
		public static final double comida_por_minuto = -0.03;
		public static final double energia_por_minuto = -0.04;
		public static final double vida_por_minuto = 0.10;

	}

	// DATOS
	public static class Directorios {

		//Principal
		public static String principal = "datos/";
		public static String introducciones = principal + "introducciones/"; //Carpeta con multiples introducciones
		public static String items = principal + "items/"; //Carpeta con todos los datos/items
	}

//	// ARCHIVOS
//	public static class Archivos {
//				
//	}
	
	// CLASE
	public static class Probabilidades {

//		// Probabilidades en %
//		int ataque_zombi = 5;
//		int encontrar_superviviente = 10;
//		int encontrar_perro = 10;

	}

}
