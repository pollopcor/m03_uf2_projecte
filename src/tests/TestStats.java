package tests;

import programa.Config;

public class TestStats {

	public static void main(String[] args) {

		//PROVAR LOS STATS Y LA CONFIGURACION DE LOS MISMOS
		
		String vidaStr = "";
		String comidaStr = "";
		String energiaStr = "";
		String tiempoStr = "";
		
		double vida = 0;
		double comida = 100;
		double energia = 100;
		
		for(int i=0;i<24;i++) {
			
			if(comida > 50)
				vida += 120 * Config.Personajes.vida_por_minuto;
			comida += 120 * Config.Personajes.comida_por_minuto;
			energia += 120 * Config.Personajes.energia_por_minuto;
			
			if(vida <0)
				vida = 0;
			else if (vida > 100)
				vida = 100;
			if(comida <0)
				comida = 0;
			else if (comida > 100)
				comida = 100;
			if(energia <0)
				energia = 0;
			else if (energia > 100)
				energia = 100;
			
			vidaStr += (int)vida + "\t";
			comidaStr += (int)comida + "\t";
			energiaStr += (int)energia + "\t";
			tiempoStr += (i+1) + "h\t";

		}
		
		
		System.out.println("   Vida: " + vidaStr);
		System.out.println(" Comida: " + comidaStr);
		System.out.println("Energia: " + energiaStr);
		System.out.println(" Tiempo: " + tiempoStr);
		
	}

}
